<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Admin - Gold Star Driving</title>
        <!-- loader-->
        <link href="{{ asset('/admin/assets/css/pace.min.css') }}" rel="stylesheet"/>
        <script src="{{ asset('/admin/assets/js/pace.min.js') }}"></script>
        <!--favicon-->
        <link rel="icon" href="{{ asset('/admin/assets/images/logostar_32.webp') }}" type="image/x-icon">
        <!-- Vector CSS -->
        {{-- <link href="{{ asset('/admin/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet"/> --}}
        <!-- simplebar CSS-->
        <link href="{{ asset('/admin/assets/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet"/>

        <!-- Bootstrap core CSS-->
        <link href="{{ asset('/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
        <!-- animate CSS-->
        <link href="{{ asset('/admin/assets/css/animate.css') }}" rel="stylesheet" type="text/css"/>
        <!-- Icons CSS-->
        <link href="{{ asset('/admin/assets/css/icons.css') }}" rel="stylesheet" type="text/css"/>
        <!-- Sidebar CSS-->
        <link href="{{ asset('/admin/assets/css/sidebar-menu.css') }}" rel="stylesheet"/>
        <!-- Custom Style-->
        <link href="{{ asset('/admin/assets/css/app-style.css') }}" rel="stylesheet"/>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        {{-- toasts alert --}}
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet" />

        

        <script src="https://cdn.ckeditor.com/4.21.0/standard/ckeditor.js"></script>
        
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        

        @yield('custom_css')
        
    </head>
    <body class="bg-theme bg-theme2">
        <div id="app">
            <main class="py-4">
                {{-- <div class="container"> --}}
                    <div id="wrapper">
                        <div class="clearfix"></div>
                        @if(\Auth::user())
                            @include('admin.sidebar')
                            @include('admin.header_bar')
                        @endif
                        @yield('content')
                    </div>
                    {{-- @if(\Auth::user())
                        @include('admin.footer')
                    @endif --}}
                {{-- </div> --}}
            </main>
        </div>

         <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('admin/assets/js/jquery.min.js') }}"></script>

        {{-- for table sorting and filtering --}}
        <script src="{{ asset('admin/assets/js/fancyTable.min.js') }}"></script>


        <script src="{{ asset('admin/assets/js/popper.min.js') }}"></script>
        <script src="{{ asset('admin/assets/js/bootstrap.min.js') }}"></script>

        
            
        <!-- simplebar js -->
        <script src="{{ asset('admin/assets/plugins/simplebar/js/simplebar.js') }}"></script>
        <!-- sidebar-menu js -->
        <script src="{{ asset('admin/assets/js/sidebar-menu.js') }}"></script>

        <!-- loader scripts -->
        {{-- <script src="{{ asset('admin/assets/js/jquery.loading-indicator.js') }}"></script> --}}
        <!-- Custom scripts -->
        <script src="{{ asset('admin/assets/js/app-script.js') }}"></script>
        <!-- Chart js -->
        
        {{-- <script src="{{ asset('admin/assets/plugins/Chart.js/Chart.min.js') }}"></script> --}}
        
        <!-- Index js -->
        {{-- <script src="{{ asset('admin/assets/js/index.js') }}"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        

        <script>
            $('input[name="contact_number"]').mask('000-000-0000');
        </script>
        
        
        <script>
            @if(Session::has('error'))
                toastr.options =
                {
                    "closeButton" : true,
                    "progressBar" : true
                }
                        toastr.error("{{ session('error') }}");
            @endif

            @if(Session::has('success'))
                toastr.options =
                {
                    "closeButton" : true,
                    "progressBar" : true
                }
                        toastr.success("{{ session('success') }}");
            @endif

            @if(Session::has('info'))
                toastr.options =
                {
                    "closeButton" : true,
                    "progressBar" : true
                }
                        toastr.info("{{ session('info') }}");
            @endif
        </script>
        <script>
            $(".searchable_table").fancyTable({
                globalSearch: true,
                matchCase:false,
            });
        </script>
        <script>
            $(".blog-tags").select2({
                tags: true,
                tagSeparators: [',', ' '],
            });
        </script>
        @yield('custom_js')
    </body>
</html>
