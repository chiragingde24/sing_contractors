@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-content">
                <div class="row row-group m-0">
                    <div class="col-12 col-lg-6 col-xl-4 border-light">
                        <div class="card-body">
                            <h5 class="text-white mb-0">{{ $bookingCount }} <span class="float-right"><i
                                        class="fa fa-shopping-cart"></i></span>
                            </h5>
                            <div class="progress my-3" style="height:3px;">
                                <div class="progress-bar" style="width:55%"></div>
                            </div>
                            <p class="mb-0 text-white small-font">Total Bookings 
                                <a href="{{ route('bookings.index') }}">
                                    <span class="float-right">
                                        <i class="fa fa-eye"></i>
                                    </span>
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-4 border-light">
                        <div class="card-body">
                            <h5 class="text-white mb-0">{{ $inquiryCount }} <span class="float-right"><i class="fa fa-question-circle"></i></span>
                            </h5>
                            <div class="progress my-3" style="height:3px;">
                                <div class="progress-bar" style="width:55%"></div>
                            </div>
                            <p class="mb-0 text-white small-font">Total Inquiries 
                                <a href="{{ route('inquires.index') }}">
                                    <span class="float-right">
                                        <i class="fa fa-eye"></i>
                                    </span>
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-4 border-light">
                        <div class="card-body">
                            <h5 class="text-white mb-0">{{ $locationCount }} <span class="float-right"><i class="fa fa-globe"></i></span>
                            </h5>
                            <div class="progress my-3" style="height:3px;">
                                <div class="progress-bar" style="width:55%"></div>
                            </div>
                            <p class="mb-0 text-white small-font">Total Locations 
                                <a href="{{ route('locations.index') }}">
                                    <span class="float-right">
                                        <i class="fa fa-eye"></i>
                                    </span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection