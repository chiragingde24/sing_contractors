@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3"><h5 class="card-title">Blogs</h5></div>
                            <div class="col-lg-9" style="padding: 15px"><a href="{{ route('blogs.create') }}" class="btn btn-warning" style="float: right;">Create Blog</a></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table searchable_table">
                                <thead>
                                    <tr class="th_color">
                                        <th scope="col"><b>#</b></th>
                                        <th scope="col"><b>Title</b></th>
                                        <th scope="col"><b>Description</b></th>
                                        <th scope="col"><b>Slug</b></th>
                                        <th scope="col"><b>Image</b></th>
                                        <th scope="col"><b>Status</b></th>
                                        <th scope="col"><b>Action</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($blogs) > 0)
                                        @foreach($blogs as $blog)
                                            @php $image = '<img width="80" height="80" src="'. asset('storage/images/blogs/'.$blog->image) . '" />'; @endphp
                                            <tr>
                                                <th scope="row">{{ $blog->id }}</th>
                                                <td>{{ $blog->title ?? "" }}</td>
                                                <td style="max-width: 300px; overflow: hidden;">{{ $blog->short_description ?? "" }}</td>
                                                <td>{{ $blog->slug ?? "" }}</td>
                                                <td>{!! $blog->image ? $image : '-' !!}</td>
                                                <td>{{ $blog->status == 1 ? "Active" : "Inactive" }}</td>
                                                <td>
                                                    <form action="{{ route('blogs.destroy',$blog->id) }}" method="Post">
                                                        <a class="btn btn-primary" href="{{ route('blogs.edit',$blog->id) }}">Edit</a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" onclick="return confirm('Do you want to delete this blog?')" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td>No Blogs Found!...</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection