@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content p-3">
                        <div class="tab-pane active" id="edit">
                            <h5>Edit Blog</h5><hr>
                            <form method="post" action="{{ route('blogs.update', $blog->id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                @php
                                    $keywords = explode(',', $blog->seo_keywords) 
                                @endphp
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" value="{{ $blog->title ?? '' }}" class="form-control" id="title" placeholder="Title">
                                        @if($errors->has('title'))
                                            <div class="error"><b>{{ $errors->first('title') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="slug">Slug</label>
                                        <input type="text" name="slug" value="{{ $blog->slug ?? '' }}" class="form-control" id="slug" placeholder="Slug" readonly>
                                        @if($errors->has('slug'))
                                            <div class="error"><b>{{ $errors->first('slug') }}</b></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="short_description">Short Description</label>
                                        <textarea name="short_description" class="form-control" rows="4" id="short_description">{{ $blog->short_description }}</textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="seo_description">Seo Description</label>
                                        <textarea name="seo_description" class="form-control" rows="4" id="seo_description">{{ $blog->seo_description }}</textarea>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="seo_keywords">Publish Date</label>
                                        <input type="date" name="publish_date" value="{{ $blog->publish_date }}" class="form-control" id="publish_date" placeholder="publish date">
                                        @if($errors->has('publish_date'))
                                            <div class="error"><b>{{ $errors->first('publish_date') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="seo_keywords">Seo Keywords</label>
                                        <select class="form-control blog-tags" name="seo_keywords[]" multiple="multiple">
                                            @if(!empty($keywords))
                                                @foreach($keywords as $keyword)
                                                    <option value="{{ $keyword }}" selected>{{ $keyword }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('seo_keywords'))
                                            <div class="error"><b>{{ $errors->first('seo_keywords') }}</b></div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="custom_script">Custom Script</label>
                                        <textarea name="custom_script" class="form-control" rows="5" id="custom_script">{{ $blog->custom_script }}</textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label><b>Please Select Blog Image</b></label>
                                        <input type="file" name="image" onchange="readURLEdit(this);" class="form-control">
                                        <img id="blah1" src="{{ asset('storage/images/blogs/' . $blog->image) }}" height="85" alt="your image" style="padding-top: 10px" />
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="description">Blog Content</label>
                                        <textarea name="description" rows="5" class="form-control" id="description">{!! $blog->description !!}</textarea>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" name="status" type="checkbox" role="switch" id="status" {{ $blog->status == 1 ? "checked" : "" }}>
                                            <label class="form-check-label" for="status">Status</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a type="button" href="{{ route('blogs.index') }}" class="btn btn-danger">Back</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('description');
</script>
@endsection

@section('custom_js')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    $("#title").on("keyup change", function(e) {
        $.get('{{ route("slug") }}',
            { 'title': $(this).val() },
            function( data ) {
            $('#slug').val(data.slug);
            }
        );
    });

    function readURLEdit(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            $('#blah1').attr('src', e.target.result).height(85);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection