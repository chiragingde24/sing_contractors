@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3"><h5 class="card-title">Schedules</h5></div>
                            <div class="col-lg-9" style="padding: 15px"><a href="{{ route('schedule.create') }}" class="btn btn-warning" style="float: right;">Create Schedule</a></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table searchable_table">
                                <thead>
                                    <tr class="th_color">
                                        <th scope="col"><b>#</b></th>
                                        <th scope="col"><b>Year & Month</b></th>
                                        <th scope="col"><b>Date</b></th>
                                        <th scope="col"><b>Days</b></th>
                                        <th scope="col"><b>Time</b></th>
                                        <th scope="col"><b>Action</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($schedules) > 0)
                                        @foreach($schedules as $schedule)
                                            <tr>
                                                <th scope="row">{{ $schedule->id }}</th>
                                                <td>{{ $schedule->year . " " . $schedule->month ?? "" }}</td>
                                                <td>{{ $schedule->date ?? "" }}</td>
                                                <td>{{ $schedule->days ?? "" }}</td>
                                                <td>{{ $schedule->time ?? "" }}</td>
                                                <td>
                                                    <form action="{{ route('schedule.destroy',$schedule->id) }}" method="Post">
                                                        <a class="btn btn-primary" href="{{ route('schedule.edit',$schedule->id) }}">Edit</a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" onclick="return confirm('Do you want to delete this schedule?')" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td>No schedules Found!...</td>
                                            <td></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="padding: 15px">{{ $schedules->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection