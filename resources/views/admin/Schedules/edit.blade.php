@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content p-3">
                        <div class="tab-pane active" id="edit">
                            <h5>Update Schedule</h5><hr>
                            <form method="post" action="{{ route('schedule.update', $schedule->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="schedule_section">
                                    <div class="form-row" style="border: 1px solid greenyellow; padding: 5px; margin-bottom: 10px;">
                                        <div class="form-group col-md-3">
                                            <label for="year">Year</label>
                                            {{-- <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="Title"> --}}
                                            <input data-date-format="yyyy" id="datepicker" value="{{ $schedule->year }}" name="year" class="form-control datepicker">
                                            @if($errors->has('year'))
                                                <div class="error"><b>{{ $errors->first('year') }}</b></div>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="month">Month</label>
                                            {{-- <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="Title"> --}}
                                            <input data-date-format="mm" id="datepicker1" value="{{ $schedule->month }}" name="month" class="form-control datepicker1">
                                            @if($errors->has('month'))
                                                <div class="error"><b>{{ $errors->first('month') }}</b></div>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="date">Date</label>
                                            {{-- <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="Title"> --}}
                                            <input type="text" id="date" name="date" value="{{ $schedule->date }}" class="form-control">
                                            @if($errors->has('date'))
                                                <div class="error"><b>{{ $errors->first('date') }}</b></div>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="days">Days</label>
                                            {{-- <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="Title"> --}}
                                            <input type="text" id="days" name="days" value="{{ $schedule->days }}" class="form-control">
                                            @if($errors->has('days'))
                                                <div class="error"><b>{{ $errors->first('days') }}</b></div>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="time">Time</label>
                                            {{-- <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="Title"> --}}
                                            <input type="text" id="time" name="time" value="{{ $schedule->time }}" class="form-control">
                                            @if($errors->has('time'))
                                                <div class="error"><b>{{ $errors->first('time') }}</b></div>
                                            @endif
                                        </div>
                                        {{-- <div class="form-group col-md-1 add_more_button">
                                            <a href="#" class="btn btn-warning add_more">+</a>
                                        </div> --}}
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Update</button>
                                <a type="button" href="{{ route('schedule.index') }}" class="btn btn-danger">Back</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('content');
</script>
@endsection

@section('custom_js')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

    let selectedYear = "";
    let thisYear = new Date().getFullYear();
    $(document).on('change',".datepicker", function(){
        selectedYear = $(this).val();
    });

    $(document).on('focus',".datepicker", function(){
        $(this).blur();
        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            startDate: new Date()
        });
    });

    $(document).on('focus',".datepicker1", function(){
        $('.datepicker1').datepicker({
            autoclose: true,
            format: "MM",
            viewMode: "months",
            minViewMode: "months",
            // startDate: new Date()
            startDate: selectedYear > thisYear ? new Date(selectedYear+"-01-01") : new Date()
        });
    });

</script>
@endsection