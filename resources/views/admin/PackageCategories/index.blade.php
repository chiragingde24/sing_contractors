@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3"><h5 class="card-title">Package Categories</h5></div>
                            <div class="col-lg-9" style="padding: 15px"><a href="{{ route('package_categories.create') }}" class="btn btn-warning" style="float: right;">Create Category</a></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr class="th_color">
                                        <th scope="col"><b>#</b></th>
                                        <th scope="col"><b>Title</b></th>
                                        <th scope="col" style="text-align: end"><b>Action</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($packageCategories) > 0)
                                        @foreach($packageCategories as $packageCategory)
                                            <tr>
                                                <th scope="row">{{ $packageCategory->id }}</th>
                                                <td>{{ $packageCategory->title ?? "" }}</td>
                                                <td style="text-align: end">
                                                    <form action="{{ route('package_categories.destroy',$packageCategory->id) }}" method="Post">
                                                        <a class="btn btn-primary" href="{{ route('package_categories.edit',$packageCategory->id) }}">Edit</a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" onclick="return confirm('Do you want to delete this package category?')" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td>No Package Catagories Found!...</td>
                                            <td></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection