<!--Start sidebar-wrapper-->
<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
    <div class="brand-logo">
     <a href="{{ route('home') }}">
      <img src="{{ asset('admin/assets/images/logo_star.webp') }}" class="logo-icon" alt="logo icon">
      <h5 class="logo-text">Gold Star Driving</h5>
    </a>
  </div>
  <ul class="sidebar-menu do-nicescrol">
        {{-- <li class="sidebar-header">MAIN NAVIGATION</li> --}}
        <li>
            <a href="{{ route('home') }}">
                <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="{{ (request()->segment(2) == 'locations') ? 'active' : '' }}">
            <a href="{{ route('locations.index') }}">
                <i class="fa-sharp fa-solid fa-map-location-dot"></i> <span>Location</span>
            </a>
        </li>
        <li class="{{ (request()->segment(2) == 'schedule') ? 'active' : '' }}">
            <a href="{{ route('schedule.index') }}">
                <i class="fa fa-server" aria-hidden="true"></i> <span>Schedule Management</span>
            </a>
        </li>
        <li class="{{ (request()->segment(2) == 'bookings') ? 'active' : '' }}">
            <a href="{{ route('bookings.index') }}">
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i> <span>Bookings</span>
            </a>
        </li>
        <li class="{{ (request()->segment(2) == 'inquires') ? 'active' : '' }}">
            <a href="{{ route('inquires.index') }}">
                <i class="fa fa-phone-square" aria-hidden="true"></i> <span>Inquires</span>
            </a>
        </li>
        <li class="{{ (request()->segment(2) == 'package_categories') ? 'active' : '' }}">
            <a href="{{ route('package_categories.index') }}">
                <i class="fa fa-list-alt" aria-hidden="true"></i> <span>Package Categories</span>
            </a>
        </li>
        <li class="{{ (request()->segment(2) == 'packages') ? 'active' : '' }}">
            <a href="{{ route('packages.index') }}">
                <i class="fa fa-usd" aria-hidden="true"></i> <span>Packages</span>
            </a>
        </li>
        <li class="{{ (request()->segment(2) == 'cms') ? 'active' : '' }}">
            <a href="{{ route('cms.index') }}">
                <i class="fa fa-cogs" aria-hidden="true"></i> <span>CMS</span>
            </a>
        </li>
        <li class="{{ (request()->segment(2) == 'faqs') ? 'active' : '' }}">
            <a href="{{ route('faqs.index') }}">
                <i class="fa fa-question" aria-hidden="true"></i> <span>FAQs</span>
            </a>
        </li>
        <li class="{{ (request()->segment(2) == 'blogs') ? 'active' : '' }}">
            <a href="{{ route('blogs.index') }}">
                <i class="fa fa-rss-square" aria-hidden="true"></i> <span>Blogs</span>
            </a>
        </li>
   </ul>
  
  </div>
  <!--End sidebar-wrapper-->