@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3"><h5 class="card-title">Bookings</h5></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table booking_table searchable_table" id="booking_table">
                                <thead>
                                    <tr class="th_color">
                                        <th scope="col"><b>#</b></th>
                                        <th scope="col"><b>Email</b></th>
                                        <th scope="col"><b>Phone</b></th>
                                        <th scope="col"><b>Message</b></th>
                                        <th scope="col"><b>Address</b></th>
                                        <th scope="col"><b>Action</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($bookings) > 0)
                                        @foreach($bookings as $booking)
                                            <tr>
                                                <th scope="row">{{ $booking->id }}</th>
                                                <td>{{ $booking->email ?? "" }}</td>
                                                <td>{{ $booking->phone ?? "" }}</td>
                                                <td style="max-width: 200px; overflow: hidden;">{{ $booking->message ?? "" }}</td>
                                                <td>{{ $booking->address ?? "" }}</td>
                                                <td><a class="btn btn-warning" href="#" id="booking_{{ $booking->id }}" data-toggle="modal" data-target="#exampleModalCenter_{{ $booking->id }}">View</a></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td>No Bookings Found!...</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="padding: 15px">{{ $bookings->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(count($bookings) > 0)
    @foreach($bookings as $booking)
        <div class="modal fade" id="exampleModalCenter_{{ $booking->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Booking data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <p><b>Name :</b> {{ $booking->name }}</p>
                    <p><b>Email :</b> {{ $booking->email }}</p>
                    <p><b>Phone :</b> {{ $booking->phone }}</p>
                    <p><b>Message :</b> {{ $booking->message }}</p>
                    <p><b>Address :</b> {{ $booking->address }}</p>
                    <p><b>Package :</b> {{ $booking->package->package_name }}</p>
                    <p><b>Time Slot :</b> {{ ($booking->schedule->year ?? '-') . ' ' . ($booking->schedule->month ?? '-') . ' ' . ($booking->schedule->date ?? '-') . ' ' . ($booking->schedule->days ?? '-') . ' ' . ($booking->schedule->time ?? '-') }}</p>
                </div>
            </div>
            </div>
        </div>
    @endforeach
@endif

@endsection