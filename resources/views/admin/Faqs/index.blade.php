@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3"><h5 class="card-title">FAQs</h5></div>
                            <div class="col-lg-9" style="padding: 15px"><a href="{{ route('faqs.create') }}" class="btn btn-warning" style="float: right;">Create FAQs</a></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table searchable_table">
                                <thead>
                                    <tr class="th_color">
                                        <th scope="col"><b>#</b></th>
                                        <th scope="col"><b>Title</b></th>
                                        <th scope="col"><b>Content</b></th>
                                        <th scope="col"><b>Action</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($faqs) > 0)
                                        @foreach($faqs as $faq)
                                            <tr>
                                                <th scope="row">{{ $faq->id }}</th>
                                                <td>{{ $faq->title ?? "" }}</td>
                                                <td style="max-width: 450px; overflow: hidden;">{{ $faq->content ?? "" }}</td>
                                                <td>
                                                    <form action="{{ route('faqs.destroy',$faq->id) }}" method="Post">
                                                        <a class="btn btn-primary" href="{{ route('faqs.edit',$faq->id) }}">Edit</a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" onclick="return confirm('Do you want to delete this faq?')" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td>No FAQs Found!...</td>
                                            <td></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="padding: 15px">{{ $faqs->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection