@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content p-3">
                        <div class="tab-pane active" id="edit">
                            <h5>Create Page</h5><hr>
                            <form method="post" action="{{ route('faqs.update', $faq->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" value="{{ $faq->title ?? "" }}" class="form-control" id="title" placeholder="Title">
                                        @if($errors->has('title'))
                                            <div class="error"><b>{{ $errors->first('title') }}</b></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Content</label>
                                        <textarea name="content" id="content">{{ $faq->content ?? "" }}</textarea>
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a type="button" href="{{ route('faqs.index') }}" class="btn btn-danger">Back</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('content');
</script>
@endsection