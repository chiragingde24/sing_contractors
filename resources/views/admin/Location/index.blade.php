@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3"><h5 class="card-title">Locations</h5></div>
                            <div class="col-lg-9" style="padding: 15px"><a href="{{ route('locations.create') }}" class="btn btn-warning" style="float: right;">Create Location</a></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table searchable_table">
                                <thead>
                                    <tr class="th_color">
                                        <th scope="col"><b>#</b></th>
                                        <th scope="col"><b>Address 1</b></th>
                                        <th scope="col"><b>Address 2</b></th>
                                        <th scope="col"><b>Country</b></th>
                                        <th scope="col"><b>State</b></th>
                                        <th scope="col"><b>City</b></th>
                                        <th scope="col"><b>Zipcode</b></th>
                                        <th scope="col"><b>Contact Number</b></th>
                                        <th scope="col"><b>Email</b></th>
                                        <th scope="col"><b>Action</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($locations) > 0)
                                        @foreach($locations as $location)
                                            <tr>
                                                <th scope="row">{{ $location->id }}</th>
                                                <td>{{ $location->address_line_1 ?? "" }}</td>
                                                <td>{{ $location->address_line_2 ?? "" }}</td>
                                                <td>{{ isset($location->country) ? $location->country->name : "-" }}</td>
                                                <td>{{ isset($location->state) ? $location->state->name : "-" }}</td>
                                                <td>{{ isset($location->city) ? $location->city->name : "-" }}</td>
                                                <td>{{ $location->zipcode ?? "" }}</td>
                                                <td>{{ $location->contact_number ?? "" }}</td>
                                                <td>{{ $location->email ?? "" }}</td>
                                                <td>
                                                    <form action="{{ route('locations.destroy',$location->id) }}" method="Post">
                                                        <a class="btn btn-primary" href="{{ route('locations.edit',$location->id) }}">Edit</a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" onclick="return confirm('Do you want to delete this location?')" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>No Locations found!...</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection