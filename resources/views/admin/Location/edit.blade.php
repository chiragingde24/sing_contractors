@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content p-3">
                        <div class="tab-pane active" id="edit">
                            <h5>Update Location</h5><hr>
                            <form method="post" action="{{ route('locations.update', $location->id) }}">
                                @csrf
                                @method('PUT')
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="address_line_1">Address Line 1</label>
                                        <input type="text" name="address_line_1" value="{{ $location->address_line_1 }}" class="form-control" id="address_line_1" placeholder="Address Line 1">
                                        @if($errors->has('address_line_1'))
                                            <div class="error"><b>{{ $errors->first('address_line_1') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="address_line_2">Address Line 2</label>
                                        <input type="text" name="address_line_2" value="{{ $location->address_line_2 }}" class="form-control" id="address_line_2" placeholder="Address Line 2">
                                        @if($errors->has('address_line_2'))
                                            <div class="error"><b>{{ $errors->first('address_line_2') }}</b></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="country_id">Country</label>
                                        <select id="country_id" name="country_id" class="form-control">
                                            <option value="" selected>Choose country...</option>
                                            @if(count($countries) > 0)
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" {{ $location->country_id == $country->id ? "selected" : "" }}>{{ $country->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('country_id'))
                                            <div class="error"><b>{{ $errors->first('country_id') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="state_id">State</label>
                                        <select id="state_id" name="state_id" class="form-control">
                                            <option value="" selected>Choose state...</option>
                                            @if(count($states) > 0)
                                                @foreach($states as $state)
                                                    <option value="{{ $state->id }}" {{ $state->id == $location->state_id ? "selected" : "" }}>{{ $state->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('state_id'))
                                            <div class="error"><b>{{ $errors->first('state_id') }}</b></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="city_id">City</label>
                                        <select id="city_id" name="city_id" class="form-control">
                                            <option value="" selected>Choose city...</option>
                                            @if(count($cities) > 0)
                                                @foreach($cities as $city)
                                                    <option value="{{ $city->id }}" {{ $city->id == $location->city_id ? "selected" : "" }}>{{ $city->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('city_id'))
                                            <div class="error"><b>{{ $errors->first('city_id') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="zipcode">Zip Code</label>
                                        <input type="text" name="zipcode" value="{{ $location->zipcode }}" class="form-control" id="zipcode" placeholder="Zip Code">
                                        @if($errors->has('zipcode'))
                                            <div class="error"><b>{{ $errors->first('zipcode') }}</b></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="contact_number">Contact Number</label>
                                        <input type="text" name="contact_number" value="{{ $location->contact_number }}" class="form-control" id="contact_number" placeholder="Contact Number">
                                        @if($errors->has('contact_number'))
                                            <div class="error"><b>{{ $errors->first('contact_number') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" class="form-control" value="{{ $location->email }}" id="email" placeholder="Email">
                                        @if($errors->has('email'))
                                            <div class="error"><b>{{ $errors->first('email') }}</b></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="main_intersections">Main Intersections</label>
                                        <input type="text" name="main_intersections" value="{{ $location->main_intersections }}" class="form-control" id="main_intersections" placeholder="Main intersections">
                                        @if($errors->has('main_intersections'))
                                            <div class="error"><b>{{ $errors->first('main_intersections') }}</b></div>
                                        @endif
                                    </div>
                                </div>
                                <button type="submit" name="location" class="btn btn-primary">Update</button>
                                <a type="button" href="{{ route('locations.index') }}" class="btn btn-danger">Back</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="tab-content p-3">
                    <div class="tab-pane active" id="edit_price">
                        <h5>Update package price for this location</h5><hr>
                        <form method="post" action="{{ route('locations.update', $location->id) }}">
                            @csrf
                            @method('PUT')
                            @if(count($packages) > 0)
                                @foreach($packages as $package)
                                    <div class="package_cat" style="padding: 15px; border: 2px solid white; margin-bottom: 10px">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>{{ $package->title }}</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            @if($package->packages && count($package->packages) > 0)
                                                @foreach($package->packages as $pack)
                                                    <div class="col-md-3 form-group">
                                                        <label>{{ $pack->package_name }}</label>
                                                        <input type="number" min="1" class="form-control" value="{{ isset($packagePrice) && !empty($packagePrice) && isset($packagePrice[$pack->id]) ? $packagePrice[$pack->id] : '' }}" name="location_package_price[{{ $location->id }}][{{ $pack->id }}]" />
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <button type="submit" name="update_location_package" value="update_location_package" class="btn btn-warning">Update Price</button>
                            <a type="button" href="{{ route('locations.index') }}" class="btn btn-danger">Back</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('#country_id').on('change', function () {
            var idCountry = this.value;
            $("#state_id").html('');
            $.ajax({
                url: '{{ url("admin/fetch-states") }}',
                type: "POST",
                data: {
                    country_id: idCountry,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function (result) {
                    $('#state_id').html('<option value="">Select State</option>');
                    $.each(result.states, function (key, value) {
                        $("#state_id").append('<option value="' + value
                            .id + '" {{ old("state_id") == '+ value.id +'? "selected": "" }}>' + value.name + '</option>');
                    });
                    $('#city_id').html('<option value="">Select City</option>');
                }
            });
        });
        $('#state_id').on('change', function () {
            var idState = this.value;
            $("#city_id").html('');
            $.ajax({
                url: "{{url('admin/fetch-cities')}}",
                type: "POST",
                data: {
                    state_id: idState,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function (res) {
                    $('#city_id').html('<option value="">Select City</option>');
                    $.each(res.cities, function (key, value) {
                        $("#city_id").append('<option value="' + value
                            .id + '" {{ old("city_id") == '+ value.id +'? "selected": "" }}>' + value.name + '</option>');
                    });
                }
            });
        });

    });
</script>