<footer class="footer">
    <div class="container">
      <div class="text-center">
        Copyright © {{ date("Y") }} Gold Star Driving
      </div>
    </div>
</footer>