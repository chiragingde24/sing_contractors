@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3"><h5 class="card-title">Packages</h5></div>
                            <div class="col-lg-9" style="padding: 15px"><a href="{{ route('packages.create') }}" class="btn btn-warning" style="float: right;">Create Package</a></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table searchable_table">
                                <thead>
                                    <tr class="th_color">
                                        <th scope="col"><b>#</b></th>
                                        <th scope="col"><b>Package Category</b></th>
                                        <th scope="col"><b>Package</b></th>
                                        <th scope="col"><b>Price</b></th>
                                        <th scope="col"><b>Certification Fee</b></th>
                                        <th scope="col"><b>HST</b></th>
                                        <th scope="col"><b>Total</b></th>
                                        <th scope="col" style="text-align: end"><b>Action</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($packages) > 0)
                                        @foreach($packages as $package)
                                            <tr>
                                                <th scope="row">{{ $package->id }}</th>
                                                <td>{{ $package->packageCategory['title'] ?? "" }}</td>
                                                <td>{{ $package->package_name ?? "" }}</td>
                                                <td>{{ $package->price ?? "" }}</td>
                                                <td>{{ $package->certification_fee ?? "" }}</td>
                                                <td>{{ $package->hst ?? "" }}</td>
                                                <td>{{ $package->total ?? "" }}</td>
                                                <td style="text-align: end">
                                                    <form action="{{ route('packages.destroy',$package->id) }}" method="Post">
                                                        <a class="btn btn-primary" href="{{ route('packages.edit',$package->id) }}">Edit</a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" onclick="return confirm('Do you want to delete this package?')" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td>No Package Found!...</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="padding: 15px">{{ $packages->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection