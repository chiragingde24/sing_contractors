@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content p-3">
                        <div class="tab-pane active" id="edit">
                            <h5>Create Package</h5><hr>
                            <form method="post" action="{{ route('packages.store') }}">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="package_categories_id">Package Category</label>
                                        <select id="package_categories_id" name="package_categories_id" class="form-control">
                                            <option value="" selected>Choose category...</option>
                                            @if(count($packageCategories) > 0)
                                                @foreach($packageCategories as $packageCategory)
                                                    <option value="{{ $packageCategory->id }}" {{ (old('package_categories_id') == $packageCategory->id ? "selected" : "") }}>{{ $packageCategory->title }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('package_categories_id'))
                                            <div class="error"><b>{{ $errors->first('package_categories_id') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="package_name">Package Name</label>
                                        <input type="text" name="package_name" value="{{ old('package_name') }}" class="form-control" id="package_name" placeholder="Package Name">
                                        @if($errors->has('package_name'))
                                            <div class="error"><b>{{ $errors->first('package_name') }}</b></div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="price">Price</label>
                                        <input type="number" step="any" name="price" min="1" value="{{ old('price') }}" class="form-control" id="price" placeholder="Price">
                                        @if($errors->has('price'))
                                            <div class="error"><b>{{ $errors->first('price') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="certification_fee">Certification Fee</label>
                                        <input type="number" step="any" name="certification_fee" min="1" value="{{ old('certification_fee') }}" class="form-control" id="certification_fee" placeholder="Certification Fee">
                                        @if($errors->has('certification_fee'))
                                            <div class="error"><b>{{ $errors->first('certification_fee') }}</b></div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="hst">HST</label>
                                        <input type="number" step="any" name="hst" min="1" value="{{ old('hst') }}" class="form-control" id="hst" placeholder="HST">
                                        @if($errors->has('hst'))
                                            <div class="error"><b>{{ $errors->first('hst') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="total">Total</label>
                                        <input type="text" name="total" value="{{ old('total') }}" class="form-control" id="total" placeholder="Total" readonly>
                                        @if($errors->has('total'))
                                            <div class="error"><b>{{ $errors->first('total') }}</b></div>
                                        @endif
                                    </div>
                                </div><br><br>

                                <h5>Package Details</h5><hr>
                                <div class="package_description_section">
                                    <div class="form-row">
                                        <div class="form-group col-md-10">
                                            <label for="pakcage_description">Description</label>
                                            <input type="text" name="pakcage_description[]" class="form-control" placeholder="Pakcage Description">
                                            @if($errors->has('pakcage_description'))
                                                <div class="error"><b>{{ $errors->first('pakcage_description') }}</b></div>
                                            @endif
                                        </div>
                                        <div class="col-md-2 add_more_button">
                                            <a href="#" class="btn btn-warning add_more">+</a>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Save</button>
                                <a type="button" href="{{ route('packages.index') }}" class="btn btn-danger">Back</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('custom_js')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $(".add_more").click(function() {
            $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
            $(".package_description_section").append(`<div class="form-row required_inp">
                <div class="form-group col-md-10">
                    <label for="pakcage_description">Description</label>
                    <input type="text" name="pakcage_description[]" class="form-control" placeholder="Pakcage Description">
                    @if($errors->has('pakcage_description'))
                        <div class="error"><b>{{ $errors->first('pakcage_description') }}</b></div>
                    @endif
                </div>
                <div class="col-md-2 inputRemove_button">
                    <a href="#" class="btn btn-danger inputRemove">-</a>
                </div>
            </div>`);
        });
        $('body').on('click','.inputRemove',function() {
            $(this).closest('.required_inp').remove()
        });

        $('body').on('keyup','#price, #certification_fee, #hst',function() {
            let price = $("#price").val();
            let certification_fee = $("#certification_fee").val();
            let hst = $("#hst").val();
            $("#total").val(parseFloat(Number(price)+Number(certification_fee)+Number(hst)).toFixed(2));
        });
    });
</script>
@endsection