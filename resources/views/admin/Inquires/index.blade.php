@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3"><h5 class="card-title">Inquires</h5></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table searchable_table">
                                <thead>
                                    <tr class="th_color">
                                        <th scope="col"><b>#</b></th>
                                        <th scope="col"><b>Name</b></th>
                                        <th scope="col"><b>Email</b></th>
                                        <th scope="col"><b>Phone</b></th>
                                        <th scope="col"><b>Message</b></th>
                                        <th scope="col"><b>Action</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($inquires) > 0)
                                        @foreach($inquires as $inquire)
                                            <tr>
                                                <th scope="row">{{ $inquire->id }}</th>
                                                <td>{{ $inquire->name ?? "" }}</td>
                                                <td>{{ $inquire->email ?? "" }}</td>
                                                <td>{{ $inquire->phone ?? "" }}</td>
                                                <td style="max-width: 500px; overflow: hidden;">{{ $inquire->message ?? "" }}</td>
                                                <td><a class="btn btn-warning" href="#" id="inquire_{{ $inquire->id }}" data-toggle="modal" data-target="#exampleModalCenter_{{ $inquire->id }}">View</a></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td>No Inquires Found!...</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="padding: 15px">{{ $inquires->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(count($inquires) > 0)
    @foreach($inquires as $inquire)
        <div class="modal fade" id="exampleModalCenter_{{ $inquire->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Inquire data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <p><b>Name :</b> {{ $inquire->name }}</p>
                    <p><b>Email :</b> {{ $inquire->email }}</p>
                    <p><b>Phone :</b> {{ $inquire->phone }}</p>
                    <p><b>Message :</b> {{ $inquire->message }}</p>
                </div>
            </div>
            </div>
        </div>
    @endforeach
@endif

@endsection