@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3"><h5 class="card-title">CMS Pages</h5></div>
                            <div class="col-lg-9" style="padding: 15px"><a href="{{ route('cms.create') }}" class="btn btn-warning" style="float: right;">Create Cms Page</a></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr class="th_color">
                                        <th scope="col"><b>#</b></th>
                                        <th scope="col"><b>Title</b></th>
                                        <th scope="col"><b>Slug</b></th>
                                        <th scope="col"><b>Content</b></th>
                                        <th scope="col"><b>Action</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($cmsPages) > 0)
                                        @foreach($cmsPages as $cms)
                                            <tr>
                                                <th scope="row">{{ $cms->id }}</th>
                                                <td>{{ $cms->title ?? "" }}</td>
                                                <td>{{ $cms->slug ?? "" }}</td>
                                                <td style="max-width: 900px; overflow: hidden;">{{ $cms->content ?? "" }}</td>
                                                <td>
                                                    <form action="{{ route('cms.destroy',$cms->id) }}" method="Post">
                                                        <a class="btn btn-primary" href="{{ route('cms.edit',$cms->id) }}">Edit</a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" onclick="return confirm('Do you want to delete this page?')" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row"></th>
                                            <td></td>
                                            <td></td>
                                            <td>No CMS Pages Found!...</td>
                                            <td></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection