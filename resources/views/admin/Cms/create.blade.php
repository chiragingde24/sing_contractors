@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content p-3">
                        <div class="tab-pane active" id="edit">
                            <h5>Create Page</h5><hr>
                            <form method="post" action="{{ route('cms.store') }}">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="Title">
                                        @if($errors->has('title'))
                                            <div class="error"><b>{{ $errors->first('title') }}</b></div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="slug">Slug</label>
                                        <input type="text" name="slug" value="{{ old('slug') }}" class="form-control" id="slug" placeholder="Slug" readonly>
                                        @if($errors->has('slug'))
                                            <div class="error"><b>{{ $errors->first('slug') }}</b></div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Content</label>
                                        <textarea name="content" id="content"></textarea>
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a type="button" href="{{ route('cms.index') }}" class="btn btn-danger">Back</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    CKEDITOR.replace('content');
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    $("#title").on("keyup change", function(e) {
        $.get('{{ route("slug") }}',
            { 'title': $(this).val() },
            function( data ) {
            $('#slug').val(data.slug);
            }
        );
    });
</script>
@endsection