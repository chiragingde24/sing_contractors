@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified">
                            {{-- <li class="nav-item">
                                            <a href="javascript:void();" data-target="#profile" data-toggle="pill"
                                                class="nav-link active"><i class="icon-user"></i> <span class="hidden-xs">Profile</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="javascript:void();" data-target="#messages" data-toggle="pill" class="nav-link"><i
                                                    class="icon-envelope-open"></i> <span class="hidden-xs">Messages</span></a>
                                        </li> --}}
                            <li class="nav-item">
                                <a href="javascript:void();" class="nav-link"><i class="icon-note"></i> <span
                                        class="hidden-xs">Change Profile</span></a>
                            </li>
                        </ul>
                        <div class="tab-content p-3">
                            {{-- <div class="tab-pane active" id="profile">
                                            <h5 class="mb-3">User Profile</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h6>About</h6>
                                                    <p>
                                                        Web Designer, UI/UX Engineer
                                                    </p>
                                                    <h6>Hobbies</h6>
                                                    <p>
                                                        Indie music, skiing and hiking. I love the great outdoors.
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6>Recent badges</h6>
                                                    <a href="javascript:void();" class="badge badge-dark badge-pill">html5</a>
                                                    <a href="javascript:void();" class="badge badge-dark badge-pill">react</a>
                                                    <a href="javascript:void();" class="badge badge-dark badge-pill">codeply</a>
                                                    <a href="javascript:void();" class="badge badge-dark badge-pill">angularjs</a>
                                                    <a href="javascript:void();" class="badge badge-dark badge-pill">css3</a>
                                                    <a href="javascript:void();" class="badge badge-dark badge-pill">jquery</a>
                                                    <a href="javascript:void();" class="badge badge-dark badge-pill">bootstrap</a>
                                                    <a href="javascript:void();" class="badge badge-dark badge-pill">responsive-design</a>
                                                    <hr>
                                                    <span class="badge badge-primary"><i class="fa fa-user"></i> 900 Followers</span>
                                                    <span class="badge badge-success"><i class="fa fa-cog"></i> 43 Forks</span>
                                                    <span class="badge badge-danger"><i class="fa fa-eye"></i> 245 Views</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <h5 class="mt-2 mb-3"><span class="fa fa-clock-o ion-clock float-right"></span> Recent
                                                        Activity
                                                    </h5>
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Abby</strong> joined ACME Project Team in
                                                                        <strong>`Collaboration`</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Gary</strong> deleted My Board1 in
                                                                        <strong>`Discussions`</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Kensington</strong> deleted MyBoard3 in
                                                                        <strong>`Discussions`</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>John</strong> deleted My Board1 in
                                                                        <strong>`Discussions`</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Skell</strong> deleted his post Look at Why this is.. in
                                                                        <strong>`Discussions`</strong>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <div class="tab-pane" id="messages">
                                            <div class="alert alert-info alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                <div class="alert-icon">
                                                    <i class="icon-info"></i>
                                                </div>
                                                <div class="alert-message">
                                                    <span><strong>Info!</strong> Lorem Ipsum is simply dummy text.</span>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-hover table-striped">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <span class="float-right font-weight-bold">3 hrs ago</span> Here is your a
                                                                link to the
                                                                latest summary report from the..
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span class="float-right font-weight-bold">Yesterday</span> There has been a
                                                                request on
                                                                your account since that was..
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span class="float-right font-weight-bold">9/10</span> Porttitor vitae
                                                                ultrices quis,
                                                                dapibus id dolor. Morbi venenatis lacinia rhoncus.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span class="float-right font-weight-bold">9/4</span> Vestibulum tincidunt
                                                                ullamcorper
                                                                eros eget luctus.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span class="float-right font-weight-bold">9/4</span> Maxamillion ais the
                                                                fix for tibulum
                                                                tincidunt ullamcorper eros.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> --}}
                            <div class="tab-pane active" id="edit">
                                <form method="post" action="{{ route('change-password') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label form-control-label">Name</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" name="name" type="text"
                                                value="{{ \Auth::user()->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label form-control-label">Email</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" name="email" type="email"
                                                value="{{ \Auth::user()->email }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label form-control-label">Old Password</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" name="old_password" type="password" value="">
                                            @if($errors->has('old_password'))
                                                <div class="error"><b>{{ $errors->first('old_password') }}</b></div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label form-control-label">New Password</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" name="new_password" type="password" value="">
                                            @if($errors->has('new_password'))
                                                <div class="error"><b>{{ $errors->first('new_password') }}</b></div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label form-control-label">Confirm password</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" name="new_password_confirmation" type="password" value="">
                                            @if($errors->has('new_password_confirmation'))
                                                <div class="error"><b>{{ $errors->first('new_password_confirmation') }}</b></div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label form-control-label"></label>
                                        <div class="col-lg-9">
                                            <input type="reset" class="btn btn-secondary" value="Cancel">
                                            <input type="submit" class="btn btn-primary" value="Save Profile">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection