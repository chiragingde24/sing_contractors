<?php
namespace Database\Seeders;
use App\Models\City;
use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $sql = public_path('dump/cities.sql');
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        City::truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $db = [
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'host' => env('DB_HOST'),
            'database' => env('DB_DATABASE')
        ];
        exec("mysql --user={$db['username']} --password={$db['password']} --host={$db['host']} --database {$db['database']} < $sql");
        \Log::info('Cities Import Done');
    }
}
