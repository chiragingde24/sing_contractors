<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('package_categories_id');
            $table->foreign('package_categories_id')->references('id')->on('package_categories')->onDelete('cascade');
            $table->string('package_name', 100)->nullable();
            $table->double('price', 8, 2)->nullable();
            $table->double('certification_fee', 8, 2)->nullable();
            $table->double('hst', 8, 2)->nullable();
            $table->double('total', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
};
