<?php

namespace Database\Factories;

use App\Models\Booking;
use App\Models\Package;
use App\Models\Schedule;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider\Address;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class BookingFactory extends Factory
{
    protected $model = Booking::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $TenDigitRandomNumber = rand(1000000000,9999999999);
        $SixDigitRandomNumber = rand(100000,999999);
        return [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'phone' => "+91 " . $TenDigitRandomNumber,
            'address' => fake()->address(),
            'zipcode' => $SixDigitRandomNumber,
            'message' => fake()->text(),
            'package_id' => fake()->randomElement(Package::pluck('id')),
            'schedule_id' => fake()->randomElement(Schedule::pluck('id')),
        ];
    }
}
