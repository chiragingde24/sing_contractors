<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Inquire;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Inquire>
 */
class InquireFactory extends Factory
{
    protected $model = Inquire::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'phone' => fake()->phoneNumber(),
            'message' => fake()->text(),
        ];
    }
}
