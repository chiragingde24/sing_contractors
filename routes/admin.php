<?php

use App\Http\Controllers\Admin\BlogsController;
use App\Http\Controllers\Admin\BookingsController;
use App\Http\Controllers\Admin\CmsController;
use App\Http\Controllers\Admin\FaqsController;
use App\Http\Controllers\Admin\InquiresController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LocationController;
use App\Http\Controllers\Admin\PackageCategoriesController;
use App\Http\Controllers\Admin\PackagesController;
use App\Http\Controllers\Admin\ScheduleController;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    Auth::routes();

    Route::post('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
    Route::get('/profile', [App\Http\Controllers\HomeController::class, 'profile'])->name('profile')->middleware('auth');

    Route::resource('locations', LocationController::class)->middleware('auth');
    Route::resource('cms', CmsController::class)->middleware('auth');
    Route::resource('faqs', FaqsController::class)->middleware('auth');
    Route::resource('inquires', InquiresController::class)->middleware('auth');
    Route::resource('package_categories', PackageCategoriesController::class)->middleware('auth');
    Route::resource('packages', PackagesController::class)->middleware('auth');
    Route::resource('schedule', ScheduleController::class)->middleware('auth');
    Route::resource('bookings', BookingsController::class)->middleware('auth');
    Route::resource('blogs', BlogsController::class)->middleware('auth');

    Route::post('fetch-states', [LocationController::class, 'fetchState'])->middleware('auth');
    Route::post('fetch-cities', [LocationController::class, 'fetchCity'])->middleware('auth');

    Route::get('slug', [CmsController::class, 'slug'])->name('slug')->middleware('auth');

    Route::post('/change-password', [App\Http\Controllers\HomeController::class, 'changePassword'])->name('change-password')->middleware('auth');
});