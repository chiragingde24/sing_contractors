<?php

namespace App\Providers;

use App\View\Composers\LocationComposer;
use App\View\Composers\CmsComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Use following code if you prefer to create a class
        // Based view composer otherwise use callback
        View::composer('front.layouts.includes.header', LocationComposer::class);
        View::composer('front.layouts.includes.footer', LocationComposer::class);
        View::composer('front.registration', LocationComposer::class);
        View::composer('front.contact_us', LocationComposer::class);
        View::composer('front.home', LocationComposer::class);
        View::composer('front.layouts.includes.footer', CmsComposer::class);
    }
}
