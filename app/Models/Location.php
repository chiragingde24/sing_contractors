<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;
    protected $fillable = ['address_line_1', 'address_line_2', 'country_id', 'state_id', 'city_id', 'zipcode', 'contact_number', 'email', 'main_intersections'];

    /**
     * Get the country that owns the location.
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the state that owns the location.
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    /**
     * Get the city that owns the location.
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
