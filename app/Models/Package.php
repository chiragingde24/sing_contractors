<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    protected $fillable = ['package_categories_id', 'package_name', 'price', 'certification_fee', 'hst', 'total'];

    /**
     * Get the package category that owns the package.
     */
    public function packageCategory()
    {
        return $this->belongsTo(PackageCategory::class, 'package_categories_id', 'id');
    }

    /**
     * Get the package category that owns the package.
     */
    public function packageDetails()
    {
        return $this->hasMany(PackageDetail::class);
    }

    /**
     * belongs to location packages.
     */
    public function cityPackagePrice()
    {
        return $this->belongsTo(CityPackage::class, 'id', 'package_id');
    }
}
