<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'email', 'phone', 'address', 'zipcode', 'message', 'city_id', 'package_id', 'schedule_id'];

    /**
     * Get the package that owns the booking.
     */
    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    /**
     * Get the schedule that owns the booking.
     */
    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }
}
