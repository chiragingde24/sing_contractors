<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\CmsPage;
use App\Models\Inquire;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $locationCount = Location::count();
        $inquiryCount = Inquire::count();
        $bookingCount = Booking::count();
        return view('home', compact('locationCount', 'inquiryCount', 'bookingCount'));
    }

    /**
     * 
     * admin profile
     */
    public function profile()
    {
        return view('admin.profile');
    }

    /**
     * 
     * update password
     */
    public function changePassword(Request $request)
    {
        if($request->old_password && $request->new_password) {
            # Validation
            $request->validate([
                'old_password' => 'required',
                'new_password' => 'required|confirmed',
            ]);


            #Match The Old Password
            if(!Hash::check($request->old_password, auth()->user()->password)){
                return back()->with("error", "Old Password Doesn't match!");
            }


            #Update the new Password
            User::whereId(auth()->user()->id)->update([
                'password' => Hash::make($request->new_password)
            ]);

            return back()->with("success", "Profile updated successfully!");
        }
        User::whereId(auth()->user()->id)->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        return back()->with("success", "Profile updated successfully!");
    }
}
