<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PackageCategory;
use Illuminate\Http\Request;

class PackageCategoriesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $packageCategories = PackageCategory::orderBy('created_at', 'desc')->get();
        return view('admin.PackageCategories.index', compact('packageCategories'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.PackageCategories.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required',
            ]);
            
            PackageCategory::create($request->post());

            return redirect()->route('package_categories.index')->with('success','package category has been created successfully.');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\PackageCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit(PackageCategory $package_category)
    {
        return view('admin.PackageCategories.edit',compact('package_category'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\PackageCategory  $location
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, PackageCategory $package_category)
    {
        try {
            $request->validate([
                'title' => 'required',
            ]);
            
            $package_category->fill($request->post())->save();

            return redirect()->route('package_categories.index')->with('success','Package Category Has Been updated successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\PackageCategory  $package_category
    * @return \Illuminate\Http\Response
    */
    public function destroy(PackageCategory $package_category)
    {
        try {
            $package_category->delete();
            return redirect()->route('package_categories.index')->with('success','Package category has been deleted successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }
}
