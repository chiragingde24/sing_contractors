<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\PackageCategory;
use App\Models\PackageDetail;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $packages = Package::with(['packageCategory'])->orderBy('created_at', 'asc')->paginate(20);
        // dd($packages->toArray());
        return view('admin.Packages.index', compact('packages'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $packageCategories = PackageCategory::orderBy('title', 'asc')->get();
        return view('admin.Packages.create', compact('packageCategories'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        
        $request->validate([
            'package_categories_id' => 'required',
            'package_name' => 'required',
            'price' => 'required|between:0,999.99',
            'certification_fee' => 'required|between:0,999.99',
            'hst' => 'required|between:0,999.99'
        ]);
        try {
            $package = Package::create($request->post());
            if($package && !empty($request->pakcage_description)) {
                foreach($request->pakcage_description as $des) {
                    PackageDetail::create(['package_description' => $des, 'package_id' => $package->id]);
                }
            }
            return redirect()->route('packages.index')->with('success','package has been created successfully.');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\PackageCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit(Package $package)
    {
        $packageCategories = PackageCategory::orderBy('title', 'asc')->get();
        return view('admin.Packages.edit',compact('package', 'packageCategories'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Package  $package
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Package $package)
    { 
        
        $request->validate([
            'package_categories_id' => 'required',
            'package_name' => 'required',
            'price' => 'required|between:0,999.99',
            'certification_fee' => 'required|between:0,999.99',
            'hst' => 'required|between:0,999.99'
        ]);
        try { 
            $package->fill($request->post())->save();
            if(!empty($request->pakcage_description)) {
                PackageDetail::where('package_id', $package->id)->delete();
                foreach($request->pakcage_description as $des) {
                    if(isset($des) && $des != null) {
                        PackageDetail::create(['package_description' => $des, 'package_id' => $package->id]);
                    }
                }
            }

            return redirect()->route('packages.index')->with('success','Package Has Been updated successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Package  $package_category
    * @return \Illuminate\Http\Response
    */
    public function destroy(Package $package)
    {
        try {
            $package->delete();
            return redirect()->route('packages.index')->with('success','Package has been deleted successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }
}
