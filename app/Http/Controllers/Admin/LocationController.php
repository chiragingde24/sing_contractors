<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\CityPackage;
use App\Models\Country;
use App\Models\Location;
use App\Models\PackageCategory;
use App\Models\State;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $locations = Location::with(['country', 'state', 'city'])->orderBy('created_at', 'desc')->get();
        return view('admin.Location.index', compact('locations'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $countries = Country::orderBy('name', 'asc')->where('id', config('global.CANADA_ID'))->get(["name", "id"]);
        return view('admin.Location.create', compact('countries'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'address_line_1' => 'required',
            'address_line_2' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'zipcode' => 'required',
            'contact_number' => 'required|regex:/^[0-9-]{12}+$/',
            'email' => 'required|email',
            'main_intersections' => 'required'
        ]);

        try {
            Location::create($request->post());
            return redirect()->route('locations.index')->with('success','Location has been created successfully.');
        } catch(\Exception $ex) {
            return redirect()->back()->withInput()->with('error', $ex->getMessage());
        }
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Company  $company
    * @return \Illuminate\Http\Response
    */
    public function edit(Location $location)
    {
        $countries = Country::orderBy('name', 'asc')->where('id', config('global.CANADA_ID'))->get(["name", "id"]);
        $states = State::where('country_id', $location->country_id)->orderBy('name', 'asc')->get(["name", "id"]);
        $cities = City::where('state_id', $location->state_id)->orderBy('name', 'asc')->get(["name", "id"]);
        $packages = PackageCategory::with(['packages'])->get(['id', 'title']);
        $locationPackages = CityPackage::where('city_id', $location->city_id)->get();
        $packagePrice = [];
        foreach($locationPackages as $packagePriceData) {
            $packagePrice[$packagePriceData->package_id] = $packagePriceData->price;
        }
        return view('admin.Location.edit',compact('location', 'countries', 'states', 'cities', 'packages', 'packagePrice'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Location  $location
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Location $location)
    {
        try {
            if(!isset($request->update_location_package)) {
                $request->validate([
                    'address_line_1' => 'required',
                    'address_line_2' => 'required',
                    'country_id' => 'required',
                    'state_id' => 'required',
                    'city_id' => 'required',
                    'zipcode' => 'required',
                    'contact_number' => 'required|regex:/^[0-9-]{12}+$/',
                    'email' => 'required|email',
                    'main_intersections' => 'required'
                ]);
                
                $location->fill($request->post())->save();
            }
            if(isset($request->update_location_package) && $request->update_location_package != null) {
                if(!empty($request->location_package_price[$location->id])) {
                    CityPackage::where('city_id', $location->city_id)->delete();
                    foreach($request->location_package_price[$location->id] as $packageId => $locationPackagePrice) {
                        if($locationPackagePrice != null && $locationPackagePrice != "") {
                            $storeValue = [
                                'city_id' => $location->city_id,
                                'package_id' => $packageId,
                                'price' => $locationPackagePrice
                            ];
                            CityPackage::create($storeValue);
                        }
                    }
                }
            }

            return redirect()->route('locations.index')->with('success','Location Has Been updated successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Location  $location
    * @return \Illuminate\Http\Response
    */
    public function destroy(Location $location)
    {
        try {
            $location->delete();
            return redirect()->route('locations.index')->with('success','Location has been deleted successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }


    /**
     * 
     * Fetch country wise states
     */
    public function fetchState(Request $request)
    {
        $data['states'] = State::where("country_id",$request->country_id)->orderBy('name', 'asc')->get(["name", "id"]);
        return response()->json($data);
    }

    /**
     * 
     * Fetch state wise cities
     */
    public function fetchCity(Request $request)
    {
        $data['cities'] = City::where("state_id",$request->state_id)->orderBy('name', 'asc')->get(["name", "id"]);
        return response()->json($data);
    }
}
