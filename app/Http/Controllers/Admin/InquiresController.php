<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Inquire;
use Illuminate\Http\Request;

class InquiresController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $inquires = Inquire::orderBy('created_at', 'desc')->paginate(20);
        return view('admin.Inquires.index', compact('inquires'));
    }
}
