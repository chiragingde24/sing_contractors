<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;

class BookingsController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $bookings = Booking::with(['package', 'schedule'])->orderBy('created_at', 'desc')->paginate(20);
        return view('admin.Bookings.index', compact('bookings'));
    }
}
