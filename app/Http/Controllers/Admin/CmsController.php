<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CmsPage;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Str;

class CmsController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $cmsPages = CmsPage::orderBy('created_at', 'desc')->get();
        return view('admin.Cms.index', compact('cmsPages'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.Cms.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required',
                'slug' => 'required',
                'content' => 'required',
            ]);
            
            CmsPage::create($request->post());

            return redirect()->route('cms.index')->with('success','page has been created successfully.');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CmsPage  $company
    * @return \Illuminate\Http\Response
    */
    public function edit(CmsPage $cm)
    {
        return view('admin.Cms.edit',compact('cm'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\CmsPage  $location
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, CmsPage $cm)
    {
        try {
            $request->validate([
                'title' => 'required',
                'slug' => 'required',
                'content' => 'required'
            ]);
            
            $cm->fill($request->post())->save();

            return redirect()->route('cms.index')->with('success','Page Has Been updated successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\CmsPage  $cms
    * @return \Illuminate\Http\Response
    */
    public function destroy(CmsPage $cm)
    {
        try {
            $cm->delete();
            return redirect()->route('cms.index')->with('success','Page has been deleted successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }


    /**
     * 
     * genrate slug by title
     */
    public function slug(Request $request)
    {
        $slug = Str::slug($request->title, '-');
        return response()->json(['slug' => $slug]);
    }
}
