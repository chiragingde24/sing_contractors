<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class BlogsController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->get();
        return view('admin.Blogs.index', compact('blogs'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.Blogs.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        
        $request->validate([
            'title' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'slug' => 'required',
            'seo_description' => 'required',
            'seo_keywords' => 'required',
            'publish_date' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ]);

        try {
            $seoKeywords = implode(',', $request->seo_keywords);


            
            $imageName = time().'.'.$request->image->extension();

            $file = $request->file('image');
            $file->storeAs('public/images/blogs/',$imageName);

            $response = [
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
                'slug' => $request->slug,
                'seo_description' => $request->seo_description,
                'seo_keywords' => $seoKeywords,
                'custom_script' => $request->custom_script ?? null,
                'publish_date' => $request->publish_date,
                'image' => $imageName,
                'status' => $request->has('status') ? 1 : 0
            ];

            Blog::create($response);

            return redirect()->route('blogs.index')->with('success','blog has been created successfully.');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Blog  $company
    * @return \Illuminate\Http\Response
    */
    public function edit(Blog $blog)
    {
        return view('admin.Blogs.edit',compact('blog'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Blog  $location
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Blog $blog)
    {
        $request->validate([
            'title' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'slug' => 'required',
            'seo_description' => 'required',
            'seo_keywords' => 'required',
            'publish_date' => 'required',
        ]);
        try {
            $image = $blog->image;
            if(isset($request->image) && $request->image != null) {
                $request->validate([
                    'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048'
                ]);
                if(Storage::disk('public')->exists('images/blogs/'.$blog->image)){
                    Storage::disk('public')->delete('images/blogs/'.$blog->image);
                }
                $imageName = time().'.'.$request->image->extension();

                $file = $request->file('image');
                $file->storeAs('public/images/blogs/',$imageName);
                
                $image = $imageName;
            }
            
            $blog->title = $request->title;
            $blog->short_description = $request->short_description;
            $blog->description = $request->description;
            $blog->slug = $request->slug;
            $blog->seo_title = $request->seo_title;
            $blog->seo_description = $request->seo_description;
            $blog->custom_script = $request->custom_script ?? null;
            $blog->publish_date = $request->publish_date;
            $blog->image = $image;
            $blog->seo_keywords = implode(',', $request->seo_keywords);
            $blog->status = $request->has('status') ? 1 : 0;
            $blog->save();

            return redirect()->route('blogs.index')->with('success','Blog Has Been updated successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Blog  $blog
    * @return \Illuminate\Http\Response
    */
    public function destroy(Blog $blog)
    {
        try {
            $blog->delete();
            return redirect()->route('blogs.index')->with('success','Blog has been deleted successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }


    /**
     * 
     * genrate slug by title
     */
    public function slug(Request $request)
    {
        $slug = Str::slug($request->title, '-');
        return response()->json(['slug' => $slug]);
    }
}
