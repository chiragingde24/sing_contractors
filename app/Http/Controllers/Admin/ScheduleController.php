<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $schedules = Schedule::orderBy('created_at', 'desc')->paginate(20);
        return view('admin.Schedules.index', compact('schedules'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.Schedules.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'year.*' => 'required',
                'month.*' => 'required',
                'date.*' => 'required',
                'days.*' => 'required',
                'time.*' => 'required'
            ]);
            if($request->year && !empty($request->year)){
                foreach($request->year as $key => $scheduleData){
                    Schedule::create([
                        'year' => $scheduleData,
                        'month' => $request->month[$key],
                        'date' => $request->date[$key],
                        'days' => $request->days[$key],
                        'time' => $request->time[$key],
                    ]);
                }
            }
            
            return redirect()->route('schedule.index')->with('success','Schedule has been created successfully.');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Schedule $schedule
    * @return \Illuminate\Http\Response
    */
    public function edit(Schedule $schedule)
    {
        return view('admin.Schedules.edit',compact('schedule'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Schedule  $schedule
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Schedule $schedule)
    {
        try {
            $request->validate([
                'year' => 'required',
                'month' => 'required',
                'date' => 'required',
                'days' => 'required',
                'time' => 'required'
            ]);
            
            $schedule->fill($request->post())->save();

            return redirect()->route('schedule.index')->with('success','Schedule Has Been updated successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Schedule  $schedule
    * @return \Illuminate\Http\Response
    */
    public function destroy(Schedule $schedule)
    {
        try {
            $schedule->delete();
            return redirect()->route('schedule.index')->with('success','Schedule has been deleted successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }
}
