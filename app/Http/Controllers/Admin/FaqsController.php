<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqsController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $faqs = Faq::orderBy('created_at', 'desc')->paginate(20);
        return view('admin.Faqs.index', compact('faqs'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.Faqs.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required',
                'content' => 'required',
            ]);
            
            Faq::create($request->post());

            return redirect()->route('faqs.index')->with('success','faq has been created successfully.');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Faq  $company
    * @return \Illuminate\Http\Response
    */
    public function edit(Faq $faq)
    {
        return view('admin.Faqs.edit',compact('faq'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Faq  $location
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Faq $faq)
    {
        try {
            $request->validate([
                'title' => 'required',
                'content' => 'required'
            ]);
            
            $faq->fill($request->post())->save();

            return redirect()->route('faqs.index')->with('success','Faq Has Been updated successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Faq  $faq
    * @return \Illuminate\Http\Response
    */
    public function destroy(Faq $faq)
    {
        try {
            $faq->delete();
            return redirect()->route('faqs.index')->with('success','Faq has been deleted successfully');
        } catch(\Exception $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }
}
